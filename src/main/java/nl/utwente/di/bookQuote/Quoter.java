package nl.utwente.di.bookQuote;

public class Quoter {

    public double getBookPrice(String isbn) {
        double isbnNumber = Double.parseDouble(isbn);
        return isbnNumber * (9.0/5.0) + 32;
    }}

